ASIS2's Kubernetes cluster deployment script
============================================

**\*Note: This tool is part of a class project\***

This script aims to ease the deployment of ASIS2's Kubernetes project. It was
developed for personal use, but if you find it useful give it a try or fork it!

Usage
-----

Use:

```sh
$ deployK8s.sh [option]
```

### Options


- `-g`: Uses `gdialog` instead of `cdialog`.
- `-h`: Prints the help message to the standard output and exits. The help
message is also printed if an unrecognized option is given.


FAQ
---


- ***My lab computer doesn't have `dialog` installed and I'm not an
administrator. What should I do?***

You can download the package at your home directory, build it and add it's
binary location to the PATH.

On CentOS: 

```sh
$ mkdir -p ~/rpm
$ yumdownloader --destdir ~/rpm --resolve dialog
$ mkdir ~/centos
$ cd ~/centos && rpm2cpio ~/rpm/dialog-<package_version>.rpm | cpio -id
$ export PATH="$HOME/centos/usr/sbin:$HOME/centos/usr/bin:$HOME/centos/bin:$PATH"
```


License
-------

This software is released under the terms of GNU GPL v3 (see LICENSE).