#!/usr/bin/env bash
# Kubernetes cluster with GlusterFS storage and Grafana monitoring deployment script.
# Author: Paul Hodgetts <paul98zgz@gmail.com>
# License: GNU GPL v3 - https://www.gnu.org/licenses/gpl-3.0.en.html

EXECUTOR="dialog"
VM_PATH="/misc/alumnos/as2/as22018/a738701/asis2"

function helpCommand {
    echo "Kubernetes cluster with GlusterFS storage and Grafana monitoring deployment script."
    echo
    echo "Use:"
    echo
    echo "      deployK8s.sh [option]"
    echo
    echo
    echo "OPTIONS"
    echo
    echo "      -g      Uses \`gdialog\` instead of \`cdialog\`."
    echo
    echo "      -h      Prints the help message to the standard output and  exits.   The"
    echo "              help message is also printed if an unrecognized option is given."
    echo
    exit 0
}

function startK8s {
    local vms=[]

    vms=$(${EXECUTOR} --title "Start K8s" \
            --checklist "Select VMs to start:" 20 51 10 \
            "routerA"  "OpenBSD router" ON\
            "mastera" "Master of the Kubernetes cluster" ON\
            "node0a" "Node0 of the Kubernetes cluster" ON\
            "node1a" "Node1 of the Kubernetes cluster" ON\
            "node2a"  "Node2 of the Kubernetes cluster" ON\
            3>&1 1>&2 2>&3 3>&-
    )
    if [[ $? == 0 ]]; then
        for vm in ${vms}; do
            virsh -c qemu:///system define ${VM_PATH}/${vm}.xml
            virsh -c qemu:///system start ${vm}
            sleep 15
        done
    fi
    exit 0
}

function stopK8s {
    local vms=[]

    vms=$(${EXECUTOR} --title "Stop K8s" \
            --checklist "Select VMs to stop:" 20 51 10 \
            "routerA"  "OpenBSD router" ON\
            "mastera" "Master of the Kubernetes cluster" ON\
            "node0a" "Node 0 of the Kubernetes cluster" ON\
            "node1a" "Node 1 of the Kubernetes cluster" ON\
            "node2a"  "Node 2 of the Kubernetes cluster" ON\
            3>&1 1>&2 2>&3 3>&-
    )
    if [[ $? == 0 ]]; then
        for vm in ${vms}; do
            virsh -c qemu:///system destroy ${vm}
            virsh -c qemu:///system undefine ${vm}
        done
    fi
    exit 0
}

function termVMs {
	local vm=""

	vm=$(${EXECUTOR} --title "Open console for VMs" \
            --radiolist "Select node to open console:" 20 51 10 \
            "routerA"  "OpenBSD router" ON\
            "mastera" "Master of the Kubernetes cluster" off\
            "node0a" "Node0 of the Kubernetes cluster" off\
            "node1a" "Node1 of the Kubernetes cluster" off\
            "node2a" "Node2 of the Kubernetes cluster" off\
            3>&1 1>&2 2>&3 3>&-
    )
	if [[ $? == 0 ]]; then
    	virt-viewer -c qemu:///system  ${vm}
    fi
    exit 0
}

function pingVMs {
    local uGroup=""

    uGroup=$(${EXECUTOR} --title "Ping VMs" \
            --radiolist "Select group to ping:" 20 51 10 \
            ""  "ALL VMs" ON\
            "master" "Master of the Kubernetes cluster" off\
            "nodes" "Nodes of the Kubernetes cluster" off\
            3>&1 1>&2 2>&3 3>&-
    )
    if [[ $? == 0 ]]; then
        clear
        u ${uGroup} p
    fi
    exit 0
}

function executeVMs {
    local uGroup=""
    local uCommand=""

    uGroup=$(${EXECUTOR} --title "Execute command on VMs" \
            --radiolist "Select group:" 20 51 10 \
            ""  "ALL VMs" ON\
            "master" "Master of the Kubernetes cluster" off\
            "nodes" "Nodes of the Kubernetes cluster" off\
            3>&1 1>&2 2>&3 3>&-
    )
    if [[ $? != 0 ]]; then
        return
    fi

    uCommand=$(${EXECUTOR} --title "Execute command on VMs" \
            --inputbox "Input command to run in ${uGroup} group:" 20 51 \
            3>&1 1>&2 2>&3 3>&-
    )
    if [[ $? == 0 ]]; then
        clear
        u ${uGroup} s "${uCommand}"
    fi
    exit 0
}

function puppetVMs {
    local uGroup=""
    local uManifests=""

    uGroup=$(${EXECUTOR} --title "Execute command on VMs" \
            --radiolist "Select group:" 20 51 10 \
            ""  "ALL VMs" ON\
            "master" "Master of the Kubernetes cluster" off\
            "nodes" "Nodes of the Kubernetes cluster" off\
            3>&1 1>&2 2>&3 3>&-
    )
    if [[ $? != 0 ]]; then
        return
    fi

    uManifests=$(${EXECUTOR} --title "Execute command on VMs" \
            --inputbox "Insert manifests to apply to ${uGroup} group:" 20 51 \
            3>&1 1>&2 2>&3 3>&-
    )
    if [[ $? == 0 ]]; then
        clear
        u ${uGroup} s "${uManifests}"
    fi
    exit 0
}

function runOption {
    local option=${1}

    case "${option}" in
        "Start") startK8s;;
        "Stop") stopK8s;;
		"Term") termVMs;;
        "Ping") pingVMs;;
        "Execute") executeVMs;;
        "Puppet") puppetVMs;;
    esac
    clear
    echo ${option}
    exit 0
}

if [[ $# > 1 ]]; then
    helpCommand
elif [[ $# == 1 ]]; then
    if [[ $1 == "-g" ]]; then
        EXECUTOR="gdialog"
    else
        helpCommand
    fi
fi

while true; do
    choice=$(${EXECUTOR} --clear --title "K8s deploy" \
            --menu "Hi, Choose an option:" 20 51 10 \
            "Start"  "Define and start VMs and Kubernetes" \
            "Stop" "Destroy and undefine running VMs" \
            "Term" "Open a local console for a VM" \
            "Ping" "Network status" \
            "Execute" "Run command on running VMs" \
            "Puppet"  "Apply Puppet manifest on running VMs" \
            3>&1 1>&2 2>&3 3>&-
    )

    retVal=$?

    case ${retVal} in
      0)
        runOption ${choice};;
      1)
        clear
        exit 0;;
      255)
        clear
        exit 0;;
    esac
done